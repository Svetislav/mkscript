# mkscript

> `mkscript` is a simple CLI tool that helps you manage your custom shell scripts.

To use 'mkscript' all you have to do is copy 'mkscript.sh' file to your scripts folder ('/usr/local/bin/scripts'). Script expects that you have a folder '/usr/local/bin/scripts' and properly connected '.bashrc' and '.bash_aliases' files in your home (~) folder.

## Usage

You can call script by issuing this command in the folder where it is saved (you can also create an alias for a command so you can call it from anywhere in the filesystem):

``` bash
$ . ./mkscript.sh
```

Notice that you have to simultaneously source and call it. Called this way, without any option, it lists files (scripts) from '/usr/local/bin/scripts' folder.

To create new script just pass script's name:

``` bash
$ . ./mkscript.sh newscript
```

This will create 'newscript.sh' in '/usr/local/bin/scripts' and open it for editing in sublime text 3. Also it will create an alias in '.bash_aliases' (alias is the same as script's name only without '.sh') so newly created script can be called from anywhere in the filesystem. If there is already script with that name it will just open it in sublime text 3 instead of creating a new one.

To delete script just pass script's name with '-d' option:

``` bash
$ . ./mkscript.sh -d newscript
```

Prompt will appear asking for confirmation.

### OPTIONS

Only one option is currently supported:

``` bash

# delete script from '/usr/local/bin/scripts'. Prompt will appear asking for confirmation.

  -d

```

## Disclaimer

This CLI tool is a small project of mine that I only tested on my machine.
I do not guarantee that it will work as it should on your own machine and do not take any responsibility for any damage that it migth cause. That being said I advise you to use it with caution.

You are free to use it, change it so it fits your needs, and share it.

For any questions or suggestions fell free to contact me.
