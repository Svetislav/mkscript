#!/bin/bash

# folder in which scripts are saved
folder="/usr/local/bin/scripts"

# aliases file
aliases=".bash_aliases"

# file to be sourced
fileBashrc=".bashrc"

# save reference to $OLDPWD
OLDPWDref=$OLDPWD

cd ~

if [ $# -lt 3 ]
  then
  if [ $# -eq 0 ]
    then
    echo "#####################################################################################################"
    echo "To create a new script pass only a name of a script to be created as an argument."
  	echo "To delete an existing script pass '-d' option as first argument and script's name as second argument."
    echo "Available scripts from the folder '$folder' are shown below."
    echo "#####################################################################################################"
    ls -la $folder
  elif [ $# -eq 1 ]
    then
    if [ -d $folder ]
      then
      if [ -f $folder/$1.sh ]
        then
        echo "Opening script '$1.sh' from '$folder'"
        sudo subl3 $folder/$1.sh
      else
        sudo touch $folder/$1.sh
        sudo chmod 777 $folder/$1.sh
        sudo echo "#!/bin/bash" >> $folder/$1.sh
        sudo echo >> $folder/$1.sh
        sudo echo 'if [ $# -eq 0 ]; then' >> $folder/$1.sh
        sudo echo "	echo \"######################\"" >> $folder/$1.sh
        sudo echo "	echo \"script explanation\"" >> $folder/$1.sh
        sudo echo "	echo \"######################\"" >> $folder/$1.sh
        sudo echo "fi" >> $folder/$1.sh
        echo "Created script '$1.sh' in '$folder'"
        sudo chmod 744 $folder/$1.sh
        sudo subl3 $folder/$1.sh
        if grep -qF "$1" $aliases
          then
          echo "Alias for '$1.sh' not created! Alias '$1' already exists."
        else
          sudo echo "alias $1='. $folder/$1.sh'" >> $aliases
          source $fileBashrc
          echo "Alias '$1' for a script '$1.sh' created!"
        fi
      fi
    else
      echo "Error! Folder '$folder' doesn't exist!"
    fi
  elif [ $# -eq 2 -a $1 = "-d" ]
    then
    if [ -f $folder/$2.sh ]
      then
      echo "Are you sure you want to delete script '$2.sh'?"
        select yn in "Yes" "No"
          do
          case $yn in
            Yes )
            sudo rm $folder/$2.sh
            sudo grep -v $2= $aliases > temp && mv temp $aliases
            unalias $2 >/dev/null 2>/dev/null
            source $fileBashrc
            echo "Script '$2.sh' deleted!"
            break;;
            No )
            echo "Script '$2.sh' not deleted!"
            break;;
          esac
        done
    else
      echo "Error! There is no script called '$2.sh' in '$folder'."
    fi
  fi
else
  echo "Error! Wrong number of arguments."
  echo "To create a new script pass only desired name as an argument."
  echo "To delete an existing script pass '-d' option as first argument and scripts name as second argument."
fi

cd $OLDPWD
OLDPWD=$OLDPWDref
